# coding=utf-8
import os
from base import readConfig
import logging
from datetime import datetime
import threading

localReadConfig = readConfig.ReadConfig()


class Log:
    def __init__(self):
        global logPath
        logPath = os.path.join(readConfig.root_path, "result", str(datetime.now().strftime("%Y%m%d%H%M%S")))
        if not os.path.exists(logPath):
            os.makedirs(logPath)
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

        # defined handler
        handler = logging.FileHandler(os.path.join(logPath, "output.log"), encoding='utf-8')
        # defined formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def get_logger(self):
        """
        get logger
        :return:
        """
        return self.logger

    def build_start_line(self, case_name):
        """
        write start line
        :return:
        """
        self.logger.info(case_name)
        self.logger.info("-->START")

    def build_end_line(self, case_name):
        """
        write end line
        :return:
        """
        self.logger.info("-->END\n")

    def build_case_line(self, case_name, code=None, msg=None, comparison=None):
        """
        write test case line
        :param case_name:
        :param code:
        :param msg:
        :return:
        """
        self.logger.info(case_name + " - Code:" + str(code) + " - msg:" + msg + " - assert:" + comparison)

    def res_info(self, status_code=None, res=None):
        """
        write test case line
        :param status_code:
        :param res:
        :return:
        """
        if status_code == 200:
            if res['code'] == 1000000:
                self.logger.info(f"请求状态：{status_code}，返回code: {res['code']}")
            else:
                self.logger.info(f"请求状态：{status_code}，返回code: {res['code']}，返回message：{res['message']}")
        else:
            self.logger.info(f"请求状态：{status_code}")

    def get_report_path(self):
        """
        get report file path
        :return:
        """
        report_path = os.path.join(logPath, "report.html")
        return report_path

    def get_log_path(self):
        """
        get test result path
        :return:
        """
        return logPath


class MyLog:
    log = None
    mutex = threading.Lock()

    def __init__(self):
        pass

    @staticmethod
    def get_log():

        if MyLog.log is None:
            MyLog.mutex.acquire()
            MyLog.log = Log()
            MyLog.mutex.release()

        return MyLog.log


if __name__ == "__main__":
    log = MyLog.get_log()
    logger = log.get_logger()
    logger.debug("test debug")
    logger.info("test info")

