# coding=utf-8

import requests
import urllib3
import os
import re
from xlrd import open_workbook
from xml.etree import ElementTree as ElementTree
from base import readConfig
from base.Log import MyLog as Log
import json

localReadConfig = readConfig.ReadConfig()
proDir = readConfig.root_path
log = Log.get_log()
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings()
caseNo = 0


def get_token():
    """
    create a token
    :return:
    """
    base_url = localReadConfig.get_http("base_url")
    uername = localReadConfig.get_user("username")
    password = localReadConfig.get_user("password")
    url = base_url + "/user/v1/user/login"
    data = {
        "phoneNum": uername,
        "password": password
    }
    headers = {
        'terminal': '2',
        'Content-Type': 'application/json'
    }
    r = requests.post(url=url, json=data, headers=headers, verify=False)
    token = r.json()['data']['token']
    localReadConfig.set_headers("token", token)
    return token


def get_value_from_return_json(json, name1, name2):
    """
    get value by key
    :param json:
    :param name1:
    :param name2:
    :return:
    """
    info = json['info']
    group = info[name1]
    value = group[name2]
    return value


def show_return_msg(response):
    """
    show msg detail
    :param response:
    :return:
    """
    url = response.url
    msg = response.json()
    print("\n请求地址：" + url)
    # 可以显示中文
    print("\n请求返回值：" + '\n' + json.dumps(msg, ensure_ascii=False, sort_keys=True, indent=4))


# ****************************** read testCase excel ********************************


def get_xls(xls_name, sheet_name):
    """
    get interface data from xls file
    :return:
    """
    cls = []
    # get xls file's path
    xlsPath = os.path.join(proDir, "testFile", 'case', xls_name)
    # open xls file
    file = open_workbook(xlsPath)
    # get sheet by name
    sheet = file.sheet_by_name(sheet_name)
    # get one sheet's rows
    nrows = sheet.nrows
    for i in range(nrows):
        if sheet.row_values(i)[0] != 'case_name':
            cls.append(sheet.row_values(i))
    return cls


def params_substitution(params, variables=None):
    if variables is None:
        variables = {}
    for i in range(0, len(params)):
        # 变量替换
        for key in re.findall(r'\${(.*?)}', params[i]):
            if key in variables:
                params[i] = params[i].replace("${" + key + "}", variables[key])
    return params


# ****************************** read SQL xml ********************************


def set_xml():
    """
    set sql xml
    :return:
    """
    if len(database) == 0:
        sql_path = os.path.join(proDir, "testFile", "SQL.xml")
        tree = ElementTree.parse(sql_path)
        for db in tree.findall("database"):
            db_name = db.get("name")
            # print(db_name)
            table = {}
            for tb in db.getchildren():
                table_name = tb.get("name")
                # print(table_name)
                sql = {}
                for data in tb.getchildren():
                    sql_id = data.get("id")
                    # print(sql_id)
                    sql[sql_id] = data.text
                table[table_name] = sql
            database[db_name] = table


def get_xml_dict(database_name, table_name):
    """
    get db dict by given name
    :param database_name:
    :param table_name:
    :return:
    """
    set_xml()
    database_dict = database.get(database_name).get(table_name)
    return database_dict


def get_sql(database_name, table_name, sql_id):
    """
    get sql by given name and sql_id
    :param database_name:
    :param table_name:
    :param sql_id:
    :return:
    """
    db = get_xml_dict(database_name, table_name)
    sql = db.get(sql_id)
    return sql


# ****************************** read interfaceURL xml ********************************


if __name__ == "__main__":
    get_token()
