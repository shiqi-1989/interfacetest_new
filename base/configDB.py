# coding=utf-8
import atexit
import pymysql
import json
import decimal, datetime
from base import readConfig
from base.Log import MyLog as Log

log = Log.get_log()
logger = log.get_logger()
localReadConfig = readConfig.ReadConfig()
host = localReadConfig.get_db("host")
username = localReadConfig.get_db("username")
password = localReadConfig.get_db("password")
port = localReadConfig.get_db("port")
database = localReadConfig.get_db("database")


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, decimal.Decimal):
            return float(obj)
        if isinstance(obj, datetime.datetime):
            return obj.strftime("%Y-%m-%d %H:%M:%S")

        super(DecimalEncoder, self).default(obj)


class OperateMysql:

    def __init__(self):
        # atexit.register(self.conn_close)
        try:
            # connect to DB
            self.db = pymysql.connect(
                host=host,
                port=int(port),
                user=username,
                passwd=password,
                db=database,
                charset='utf8',
                cursorclass=pymysql.cursors.DictCursor
            )
            # create cursor
            self.cursor = self.db.cursor()
            print("Connect DB successfully!")
            logger.info("Connect DB successfully!")
        except ConnectionError as e:
            logger.error(str(e))

    def select_one_data(self, sql):
        """
        查询第一条数据
        """
        try:
            # 执行 sql 语句
            self.cursor.execute(sql)
            self.db.commit()
        except Exception as e:
            print(f"执行sql异常：{str(e)}")
        else:
            # 获取查询到的第一条数据
            first_data = self.cursor.fetchone()
            # print(first_data)
            # 将返回结果转换成 str 数据格式，禁用acsii编码
            first_data = json.dumps(first_data, cls=DecimalEncoder, ensure_ascii=False)
            # print(first_data)
            # self.connect_interface_testing.close()
            return first_data

    def select_all_data(self, sql):
        """
        查询结果集
        """
        try:
            self.cursor.execute(sql)
            self.db.commit()
        except Exception as e:
            print(f"执行sql异常：{str(e)}")
            logger.error(f"执行sql异常：{str(e)}")
        else:
            first_data = self.cursor.fetchall()
            first_data = json.dumps(first_data, cls=DecimalEncoder, ensure_ascii=False)
            # self.connect_interface_testing.close()
            return first_data

    def del_data(self, sql):
        """
        删除数据
        """
        res = ''
        try:
            # 执行SQL语句
            result = self.cursor.execute(sql)
            # print(result)
            if result != 0:
                # 提交修改
                self.db.commit()
                res = '删除成功'
            else:
                res = '没有要删除的数据'
        except Exception as e:
            # 发生错误时回滚
            self.db.rollback()
            res = f'删除失败{str(e)},执行回滚！'
        logger.info(f"{res}")
        return res

    def insert_data(self, sql, data):
        """
        新增数据
        """

        try:
            self.cursor.execute(sql, data)
            self.db.commit()
            res = f"{data}, 新增成功1"
        except Exception as e:
            res = f"新增失败！， {str(e)}"
        logger.info(f"{res}")
        return res

    def conn_close(self):
        # 关闭数据库
        self.cursor.close()
        self.db.close()
        print("Close DB successfully!")
        logger.info("Close DB successfully!")


if __name__ == "__main__":
    om = OperateMysql()
    sql = ""
    data = om.select_all_data(sql)
    print(data)
    print(type(data))
