# coding:utf-8

import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime
import threading
from base import readConfig
from base.Log import MyLog
import zipfile
import glob

localReadConfig = readConfig.ReadConfig()


class Email:
    def __init__(self):
        global host, user, password, port, sender, title
        host = localReadConfig.get_email("mail_host")
        user = localReadConfig.get_email("mail_user")
        password = localReadConfig.get_email("mail_pass")
        port = localReadConfig.get_email("mail_port")
        sender = localReadConfig.get_email("sender")
        title = localReadConfig.get_email("subject")
        self.on_off = localReadConfig.get_email("on_off")
        # get receiver list
        self.receiver = localReadConfig.get_email("receiver").split(',')

        # defined email subject
        date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.subject = "接口测试报告" + " " + date

        self.log = MyLog.get_log()
        self.logger = self.log.get_logger()
        self.msg = MIMEMultipart('related')

    def config_header(self):
        """
        defined email header include subject, sender and receiver
        :return:
        """
        self.msg['subject'] = self.subject
        self.msg['from'] = sender
        self.msg['to'] = ";".join(self.receiver)

    def config_html(self):
        """
        Splicing HTML and mailbox signatures
        :return:
        """
        p = '''
        <h1>Dear all</h1>
        '''
        html_path = self.log.get_report_path()
        with open(html_path, 'r', encoding='UTF-8') as f:
            aa = p + f.read()
        self.msg.attach(MIMEText(aa, "html", "utf-8"))

    def config_file(self):
        """
        config email file
        :return:
        """
        # if the file content is not null, then config the email file
        if self.check_file():
            # report.html Parent Path
            log_path = self.log.get_log_path()
            # zip file   read all fiels
            files = glob.glob(log_path + '\\*')
            zip_path = os.path.join(readConfig.root_path, "result", "test.zip")

            f = zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED)
            for file in files:
                # 修改压缩文件的目录结构
                f.write(file, '/report/' + os.path.basename(file))
            f.close()

            with open(zip_path, 'rb') as fp:
                zip_file = fp.read()
            attachment = MIMEText(zip_file, 'base64', 'utf-8')
            attachment['Content-Type'] = 'application/octet-stream'
            attachment['Content-Disposition'] = 'attachment; filename="test.zip"'
            self.msg.attach(attachment)

    def check_file(self):
        """
        check test report
        :return:
        """
        html_path = self.log.get_report_path()
        if os.path.isfile(html_path) and not os.stat(html_path) == 0:
            return True
        else:
            return False

    def send_email(self):
        """
        send email
        :return:
        """
        if self.on_off == 'on':
            print("发送邮件！")
            self.config_header()
            self.config_html()
            self.config_file()
            try:
                smtp = smtplib.SMTP(host, port)
                # smtp.ehlo()
                # smtp.starttls()
                smtp.login(user, password)
                smtp.sendmail(sender, self.receiver, self.msg.as_string())
                smtp.quit()
                print("The test report has send to developer by email.")
                print(f"The receiver includes：{self.receiver}")
                self.logger.info("The test report has send to developer by email.")
            except Exception as e:
                print(str(e))
                self.logger.error(str(e))
        elif self.on_off == 'off':
            print("Doesn't send report email to developer.")
            self.logger.info("Doesn't send report email to developer.")
        else:
            self.logger.info("Unknow state.")


class MyEmail:
    email = None
    mutex = threading.Lock()

    def __init__(self):
        pass

    @staticmethod
    def get_email():
        if MyEmail.email is None:
            MyEmail.mutex.acquire()
            MyEmail.email = Email()
            MyEmail.mutex.release()
        return MyEmail.email


if __name__ == "__main__":
    email = MyEmail.get_email()
    email.send_email()
