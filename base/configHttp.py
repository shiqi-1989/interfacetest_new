# coding=utf-8
import requests
import json
from base import readConfig
from base.Log import MyLog as Log
import urllib3

urllib3.disable_warnings()
localReadConfig = readConfig.ReadConfig()


class ConfigHttp:

    def __init__(self):
        self.base_url = localReadConfig.get_http("base_url")
        self.timeout = float(localReadConfig.get_http("timeout"))
        self.log = Log.get_log()
        self.logger = self.log.get_logger()
        # self.token = localReadConfig.get_headers("token")

    def replace_none(self, data_json):
        for i in data_json:
            if data_json[i] is None:
                data_json[i] = 'null'

    def set_url(self, path):
        url = self.base_url + path
        return url

    def set_headers(self, header):
        header = eval(header)
        return header

    def set_data(self, data):
        data = eval(data)
        return data

    def get_method(self, url, headers=None, data=None):
        try:
            res = requests.get(url=url, params=data, headers=headers, verify=False, timeout=self.timeout)
            status_code = res.status_code
            res_json = res.json()
            return status_code, res_json  # 返回响应码，响应内容
        except Exception as e:
            self.logger.error("Error:%s" % e)

    def post_method(self, url, headers=None, data=None, files=None):
        try:
            if files:
                res = requests.post(url=url, files=files, data=data, headers=headers, verify=False,
                                    timeout=self.timeout)
            else:
                res = requests.post(url=url, json=data, headers=headers, verify=False, timeout=self.timeout)
            status_code = res.status_code
            res_json = res.json()
            return status_code, res_json  # 返回响应码，响应内容
        except Exception as e:
            self.logger.error("Error:%s" % e)

    def put_method(self, url, headers=None, data=None):
        try:
            res = requests.put(url=url, data=json.dumps(data), headers=headers, verify=False, timeout=self.timeout)
            status_code = res.status_code
            res_json = res.json()
            return status_code, res_json  # 返回响应码，响应内容
        except Exception as e:
            self.logger.error("Error:%s" % e)

    def delete_method(self, url, headers=None, data=None):
        try:
            res = requests.delete(url=url, data=json.dumps(data), headers=headers, verify=False, timeout=self.timeout)
            status_code = res.status_code
            res_json = res.json()
            return status_code, res_json  # 返回响应码，响应内容
        except Exception as e:
            self.logger.error("Error:%s" % e)

    def http_method(self, method, path, headers=None, data=None, files=None):
        """判断请求方法
        :param method: 请求方法
        :param path: 接口路径
        :param data: 请求数据
        :param headers: 请求头
        :return:
        """

        url = self.set_url(path)
        print(f'2、请求url：\n{method} {url}')
        headers = self.set_headers(headers)
        print(
            f"3、请求header：\n{json.dumps(headers, ensure_ascii=False, sort_keys=True, indent=2).replace('null', 'None')}")
        data = self.set_data(data)
        print(f"4、body参数：\n{json.dumps(data, ensure_ascii=False, sort_keys=True, indent=2).replace('null', 'None')}")
        if method == 'GET':
            status_code, res_json = self.get_method(url, headers, data)
        elif method == 'POST':
            status_code, res_json = self.post_method(url, headers, data, files)
        elif method == 'PUT':
            status_code, res_json = self.put_method(url, headers, data)
        else:
            status_code, res_json = self.delete_method(url, headers, data)
        self.replace_none(res_json)
        print(f'5、响应结果：\n{json.dumps(res_json, ensure_ascii=False, sort_keys=True, indent=2)}')
        return status_code, res_json  # 对json数据进行格式化输出


if __name__ == "__main__":
    a = ConfigHttp()
    b = a.set_headers("None")
    print(b)
    # print("ConfigHTTP")
