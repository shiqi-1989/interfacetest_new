# coding=utf-8
import os
import codecs
import configparser

# 根路径
root_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
configPath = os.path.join(root_path, "config.ini")


class ReadConfig:
    def __init__(self):
        with open(configPath, encoding='utf-8') as fd:
            data = fd.read()
            #  remove BOM 检测及消除BOM
            if data[:3] == codecs.BOM_UTF8:
                data = data[3:]
                with codecs.open(configPath, "w") as file:
                    file.write(data)

        self.cf = configparser.ConfigParser()
        self.cf.read(configPath, encoding='utf-8')

    def get_email(self, name):
        value = self.cf.get("EMAIL", name)
        return value

    def get_http(self, name):
        value = self.cf.get("HTTP", name)
        return value

    def get_headers(self, name):
        value = self.cf.get("HEADERS", name)
        return value

    def set_headers(self, name, value):
        self.cf.set("HEADERS", name, value)
        with open(configPath, 'w+') as f:
            self.cf.write(f)

    def get_url(self, name):
        value = self.cf.get("URL", name)
        return value

    def get_db(self, name):
        value = self.cf.get("DATABASE", name)
        return value

    def get_user(self, name):
        value = self.cf.get("USER", name)
        return value


if __name__ == '__main__':

    PHONE = ReadConfig().get_headers('token')
    url = ReadConfig().get_http('base_url')

    print(PHONE, url)