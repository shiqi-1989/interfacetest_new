# coding=utf-8
import webbrowser
import os
import unittest
from base.Log import MyLog as Log
from base import HTMLTestRunner_PY3, readConfig as readConfig
from base.configEmail import MyEmail

localReadConfig = readConfig.ReadConfig()
log = Log.get_log()
logger = log.get_logger()
report_Path = log.get_report_path()
print(f"report_Path: {report_Path}")


class AllTest:
    def __init__(self):
        self.caseListFile = os.path.join(readConfig.root_path, "caselist.txt")
        self.caseFile = os.path.join(readConfig.root_path, "testCase")
        self.caseList = []
        self.email = MyEmail.get_email()

    def set_case_list(self):
        """
        set case list
        :return:
        """
        with open(self.caseListFile) as fb:
            for value in fb.readlines():
                data = str(value).replace("\n", "")
                if data != '' and not data.startswith("#"):
                    self.caseList.append(data)
                    logger.info(f"收集用例： {data}")

    def set_case_suite(self):
        """
        set case suite
        :return:
        """
        self.set_case_list()  # 获取可以执行的case列表
        test_suite = unittest.TestSuite()
        if self.caseList:
            for case in self.caseList:
                case_name = case.split("/")[-1]
                print(case_name + ".py")
                discover = unittest.defaultTestLoader.discover(self.caseFile, pattern=case_name + '.py',
                                                               top_level_dir=None)
                test_suite.addTest(discover)
            return test_suite
        return None

    def run(self):
        """
        run test
        :return:
        """
        try:
            suit = self.set_case_suite()
            if suit is not None:
                logger.info("********TEST START********")
                with open(report_Path, 'wb') as fp:
                    runner = HTMLTestRunner_PY3.HTMLTestRunner(stream=fp, verbosity=2,
                                                               title='Test Report（测试报告）',
                                                               description="测试描述！")
                    runner.run(suit)
            else:
                logger.info("Have no case to test.")
        except Exception as e:
            logger.error(str(e))
        finally:
            logger.info("*********TEST END*********")
            self.email.send_email()
        # 生成报告后自动在浏览器打开
        webbrowser.open(report_Path, new=0)


if __name__ == '__main__':
    obj = AllTest()
    obj.run()
