import requests
import json


def compare_json_data(A, B, xpath='.'):
    if isinstance(A, list) and isinstance(B, list):
        for i in range(len(A)):
            try:
                compare_json_data(A[i], B[i], xpath + '[%s]' % str(i))
            except:
                print('▇▇▇▇▇ A中的%s[%s]未在B中找到' % (xpath, i))
    if isinstance(A, dict) and isinstance(B, dict):
        for i in A:
            print(i)
            try:
                B[i]
            except:
                print('▇▇▇▇▇ A中的%s/%s 未在B中找到' % (xpath, i))
                continue
            if not (isinstance(A.get(i), (list, dict)) or isinstance(B.get(i), (list, dict))):
                if type(A.get(i)) != type(B.get(i)):
                    print('▇▇▇▇▇ 类型不同参数在[A]中的绝对路径:  %s/%s  ►►► A is %s, B is %s ' % (
                    xpath, i, type(A.get(i)), type(B.get(i))))
                elif A.get(i) != B.get(i):
                    print('▇▇▇▇▇ 仅内容不同参数在[A]中的绝对路径:  %s/%s  ►►► A is %s, B is %s ' % (xpath, i, A.get(i), B.get(i)))
                continue
            print(A.get(i))
            compare_json_data(A.get(i), B.get(i), xpath + '/' + str(i))
        return
    if type(A) != type(B):
        print('▇▇▇▇▇ 类型不同参数在[A]中的绝对路径:  %s  ►►► A is %s, B is %s ' % (xpath, type(A), type(B)))
    elif A != B and type(A) is not list:
        print('▇▇▇▇▇ 仅内容不同参数在[A]中的绝对路径:  %s  ►►► A is %s, B is %s ' % (xpath, A, B))


A = {
    "code": 1000001,
    "data": [
        {
            "classStage": "课前练习",
            "classStageIcon": "https://tiku.huatu.com/cdn/images/ic/20200903/48241a5cc98e42b1841b952e48af309b.png",
            "stageTasks": [
                {
                    "completeNum": 1,
                    "hasCorrect": 0,
                    "id": 57,
                    "isRead": 0,
                    "isUnlock": 1,
                    "name": "课前练习任务1",
                    "source": 1,
                    "totalNum": 5,
                    "type": "课后作业",
                    "unlockTime": "09月07日 星期一 00:00"
                },
                {
                    "completeNum": 0,
                    "hasCorrect": 0,
                    "id": 58,
                    "isRead": 0,
                    "isUnlock": 1,
                    "name": "课前练习任务2-真题",
                    "source": 2,
                    "totalNum": 4,
                    "type": "真题实战",
                    "unlockTime": "09月07日 星期一 00:00"
                },
                {
                    "completeNum": 0,
                    "hasCorrect": 0,
                    "id": 59,
                    "isRead": 1,
                    "isUnlock": 1,
                    "name": "课前练习任务3-图书",
                    "source": 3,
                    "taskItem": {
                        "source": 3,
                        "taskId": 59,
                        "taskRecordId": 387,
                        "taskStatus": 10,
                        "taskType": 1,
                        "title": "来源图书测试任务"
                    },
                    "totalNum": 1,
                    "type": "课前预习",
                    "unlockTime": "09月07日 星期一 00:00"
                },
                {
                    "completeNum": 1,
                    "hasCorrect": 0,
                    "id": 60,
                    "isRead": 1,
                    "isUnlock": 1,
                    "name": "课前练习任务4-全真考场",
                    "source": 4,
                    "taskItem": {
                        "source": 4,
                        "taskId": 60,
                        "taskRecordId": 388,
                        "taskStatus": 20,
                        "taskType": 1
                    },
                    "totalNum": 1,
                    "type": "课前预习",
                    "unlockTime": "09月07日 星期一 00:00"
                },
                {
                    "completeNum": 1,
                    "hasCorrect": 0,
                    "id": 61,
                    "isRead": 1,
                    "isUnlock": 1,
                    "name": "课前练习任务5-视频",
                    "source": 5,
                    "taskItem": {
                        "domain": "htcs",
                        "materialId": 29,
                        "objectRecordId": 29,
                        "source": 5,
                        "taskId": 61,
                        "taskItemId": 176,
                        "taskRecordId": 149,
                        "taskStatus": 20,
                        "taskType": 1,
                        "token": "oovpXAsSC4c7H_Jmfnz5Avcamvbi2szNBhlvGwI0wE2bnAU4so27QjTSEzZrILF4",
                        "videoId": "68266255",
                        "videoType": 1
                    },
                    "totalNum": 1,
                    "type": "课前预习",
                    "unlockTime": "09月07日 星期一 00:00"
                },
                {
                    "completeNum": 1,
                    "correctContent": "gggggg",
                    "hasCorrect": 1,
                    "id": 118,
                    "isRead": 1,
                    "isUnlock": 1,
                    "name": "作业资料支持批改",
                    "source": 1,
                    "taskItem": {
                        "isOffline": 0,
                        "materialId": 203,
                        "mediaType": 2,
                        "objectRecordId": 11675,
                        "source": 1,
                        "taskId": 118,
                        "taskItemId": 362,
                        "taskRecordId": 126,
                        "taskStatus": 40,
                        "taskType": 2
                    },
                    "totalNum": 1,
                    "type": "课后作业",
                    "unlockTime": "09月09日 星期三 00:00"
                },
                {
                    "completeNum": 0,
                    "hasCorrect": 0,
                    "id": 121,
                    "isRead": 0,
                    "isUnlock": 1,
                    "name": "套题测试",
                    "source": 2,
                    "totalNum": 2,
                    "type": "课后作业",
                    "unlockTime": "09月09日 星期三 00:00"
                },
                {
                    "completeNum": 0,
                    "hasCorrect": 0,
                    "id": 158,
                    "isRead": 0,
                    "isUnlock": 1,
                    "name": "新资料任务删除测试",
                    "source": 1,
                    "totalNum": 2,
                    "type": "课后作业",
                    "unlockTime": "09月11日 星期五 10:35"
                },
                {
                    "completeNum": 0,
                    "hasCorrect": 0,
                    "id": 120,
                    "isRead": 1,
                    "isUnlock": 0,
                    "name": "未解锁的任务",
                    "source": 1,
                    "taskItem": {
                        "isOffline": 1,
                        "materialId": 207,
                        "mediaType": 3,
                        "source": 1,
                        "taskId": 120,
                        "taskItemId": 365,
                        "taskStatus": 0,
                        "taskType": 1
                    },
                    "totalNum": 1,
                    "type": "课前预习",
                    "unlockTime": "09月24日 星期四 00:00"
                }
            ]
        },
        {
            "classStage": "正课学习",
            "classStageIcon": "https://tiku.huatu.com/cdn/images/ic/20200903/d93498fb454a43339d13b4691bcffb15.png",
            "stageTasks": [
                {
                    "completeNum": 0,
                    "hasCorrect": 0,
                    "id": 62,
                    "isRead": 0,
                    "isUnlock": 1,
                    "name": "正在学习-资料",
                    "source": 1,
                    "taskItem": {
                        "isOffline": 0,
                        "materialId": 203,
                        "mediaType": 2,
                        "source": 1,
                        "taskId": 62,
                        "taskItemId": 179,
                        "taskStatus": 0,
                        "taskType": 2
                    },
                    "totalNum": 1,
                    "type": "课后作业",
                    "unlockTime": "09月07日 星期一 00:00"
                },
                {
                    "completeNum": 1,
                    "hasCorrect": 0,
                    "id": 63,
                    "isRead": 1,
                    "isUnlock": 1,
                    "name": "正在学习-视频",
                    "source": 5,
                    "taskItem": {
                        "domain": "htcs",
                        "materialId": 30,
                        "objectRecordId": 30,
                        "source": 5,
                        "taskId": 63,
                        "taskItemId": 180,
                        "taskRecordId": 156,
                        "taskStatus": 20,
                        "taskType": 2,
                        "token": "Y5v8cJ7Wr3w7H_Jmfnz5ApFD1dEj3mHNBhlvGwI0wE2bnAU4so27QjTSEzZrILF4",
                        "videoId": "68267961",
                        "videoType": 1
                    },
                    "totalNum": 1,
                    "type": "课后作业",
                    "unlockTime": "09月07日 星期一 18:15"
                },
                {
                    "completeNum": 0,
                    "hasCorrect": 0,
                    "id": 126,
                    "isRead": 0,
                    "isUnlock": 1,
                    "name": "图书任务再测试",
                    "source": 3,
                    "taskItem": {
                        "source": 3,
                        "taskId": 126,
                        "taskStatus": 0,
                        "taskType": 2,
                        "title": "图书图书图书图书图书图书图书图书图书图书图书图书图书图书图书"
                    },
                    "totalNum": 1,
                    "type": "课后作业",
                    "unlockTime": "09月09日 星期三 00:00"
                }
            ]
        },
        {
            "classStage": "课后学习",
            "classStageIcon": "https://tiku.huatu.com/cdn/images/ic/20200903/c7e387ebbb9343428b015a9e4b100675.png",
            "stageTasks": [
                {
                    "completeNum": 0,
                    "hasCorrect": 0,
                    "id": 324,
                    "isRead": 0,
                    "isUnlock": 1,
                    "name": "单任务 未上线",
                    "source": 1,
                    "taskItem": {
                        "isOffline": 1,
                        "materialId": 207,
                        "mediaType": 3,
                        "source": 1,
                        "taskId": 324,
                        "taskItemId": 751,
                        "taskStatus": 0,
                        "taskType": 2
                    },
                    "totalNum": 1,
                    "type": "课后作业",
                    "unlockTime": "09月16日 星期三 14:11"
                },
                {
                    "completeNum": 0,
                    "hasCorrect": 0,
                    "id": 325,
                    "isRead": 0,
                    "isUnlock": 1,
                    "name": "多任务下线",
                    "source": 1,
                    "totalNum": 2,
                    "type": "课后作业",
                    "unlockTime": "09月16日 星期三 14:12"
                }
            ]
        }
    ]
}

B = {
    "data": [{
        "classStage": "课前练习",
        "classStageIcon": "https://tiku.huatu.com/cdn/images/ic/20200903/48241a5cc98e42b1841b952e48af309b.png",
        "stageTasks": [{
            "id": 57,
            "type": "课后作业",
            "source": 1,
            "unlockTime": "09月07日 星期一 00:00",
            "isUnlock": 1,
            "name": "课前练习任务1",
            "completeNum": 1,
            "totalNum": 5,
            "hasCorrect": 0,
            "isRead": 0
        }, {
            "id": 58,
            "type": "真题实战",
            "source": 2,
            "unlockTime": "09月07日 星期一 00:00",
            "isUnlock": 1,
            "name": "课前练习任务2-真题",
            "completeNum": 0,
            "totalNum": 4,
            "hasCorrect": 0,
            "isRead": 0
        }, {
            "id": 59,
            "type": "课前预习",
            "source": 3,
            "unlockTime": "09月07日 星期一 00:00",
            "isUnlock": 1,
            "name": "课前练习任务3-图书",
            "completeNum": 0,
            "totalNum": 1,
            "hasCorrect": 0,
            "isRead": 1,
            "taskItem": {
                "taskRecordId": 387,
                "taskId": 59,
                "title": "来源图书测试任务",
                "source": 3,
                "taskStatus": 10,
                "taskType": 1
            }
        }, {
            "id": 60,
            "type": "课前预习",
            "source": 4,
            "unlockTime": "09月07日 星期一 00:00",
            "isUnlock": 1,
            "name": "课前练习任务4-全真考场",
            "completeNum": 1,
            "totalNum": 1,
            "hasCorrect": 0,
            "isRead": 1,
            "taskItem": {
                "taskRecordId": 388,
                "taskId": 60,
                "source": 4,
                "taskStatus": 20,
                "taskType": 1
            }
        }, {
            "id": 61,
            "type": "课前预习",
            "source": 5,
            "unlockTime": "09月07日 星期一 00:00",
            "isUnlock": 1,
            "name": "课前练习任务5-视频",
            "completeNum": 1,
            "totalNum": 1,
            "hasCorrect": 0,
            "isRead": 1,
            "taskItem": {
                "taskRecordId": 149,
                "taskId": 61,
                "taskItemId": 176,
                "objectRecordId": 29,
                "materialId": 29,
                "videoType": 1,
                "videoId": "68266255",
                "token": "oovpXAsSC4c7H_Jmfnz5Avcamvbi2szNBhlvGwI0wE2bnAU4so27QjTSEzZrILF4",
                "domain": "htcs",
                "source": 5,
                "taskStatus": 20,
                "taskType": 1
            }
        }, {
            "id": 118,
            "type": "课后作业",
            "source": 1,
            "unlockTime": "09月09日 星期三 00:00",
            "isUnlock": 1,
            "name": "作业资料支持批改",
            "completeNum": 1,
            "totalNum": 1,
            "hasCorrect": 1,
            "correctContent": "gggggg",
            "isRead": 1,
            "taskItem": {
                "taskRecordId": 126,
                "taskId": 118,
                "taskItemId": 362,
                "objectRecordId": 11675,
                "materialId": 203,
                "mediaType": 2,
                "source": 1,
                "taskStatus": 40,
                "taskType": 2,
                "isOffline": 0
            }
        }, {
            "id": 121,
            "type": "课后作业",
            "source": 2,
            "unlockTime": "09月09日 星期三 00:00",
            "isUnlock": 1,
            "name": "套题测试",
            "completeNum": 0,
            "totalNum": 2,
            "hasCorrect": 0,
            "isRead": 0
        }, {
            "id": 158,
            "type": "课后作业",
            "source": 1,
            "unlockTime": "09月11日 星期五 10:35",
            "isUnlock": 1,
            "name": "新资料任务删除测试",
            "completeNum": 0,
            "totalNum": 2,
            "hasCorrect": 0,
            "isRead": 0
        }, {
            "id": 120,
            "type": "课前预习",
            "source": 1,
            "unlockTime": "09月24日 星期四 00:00",
            "isUnlock": 0,
            "name": "未解锁的任务",
            "completeNum": 0,
            "totalNum": 1,
            "hasCorrect": 0,
            "isRead": 1,
            "taskItem": {
                "taskId": 120,
                "taskItemId": 365,
                "materialId": 207,
                "mediaType": 3,
                "source": 1,
                "taskStatus": 0,
                "taskType": 1,
                "isOffline": 1
            }
        }]
    }, {
        "classStage": "正课学习",
        "classStageIcon": "https://tiku.huatu.com/cdn/images/ic/20200903/d93498fb454a43339d13b4691bcffb15.png",
        "stageTasks": [{
            "id": 62,
            "type": "课后作业",
            "source": 1,
            "unlockTime": "09月07日 星期一 00:00",
            "isUnlock": 1,
            "name": "正在学习-资料",
            "completeNum": 0,
            "totalNum": 1,
            "hasCorrect": 0,
            "isRead": 0,
            "taskItem": {
                "taskId": 62,
                "taskItemId": 179,
                "materialId": 203,
                "mediaType": 2,
                "source": 1,
                "taskStatus": 0,
                "taskType": 2,
                "isOffline": 0
            }
        }, {
            "id": 63,
            "type": "课后作业",
            "source": 5,
            "unlockTime": "09月07日 星期一 18:15",
            "isUnlock": 1,
            "name": "正在学习-视频",
            "completeNum": 1,
            "totalNum": 1,
            "hasCorrect": 0,
            "isRead": 1,
            "taskItem": {
                "taskRecordId": 156,
                "taskId": 63,
                "taskItemId": 180,
                "objectRecordId": 30,
                "materialId": 30,
                "videoType": 1,
                "videoId": "68267961",
                "token": "Y5v8cJ7Wr3w7H_Jmfnz5ApFD1dEj3mHNBhlvGwI0wE2bnAU4so27QjTSEzZrILF4",
                "domain": "htcs",
                "source": 5,
                "taskStatus": 20,
                "taskType": 2
            }
        }, {
            "id": 126,
            "type": "课后作业",
            "source": 3,
            "unlockTime": "09月09日 星期三 00:00",
            "isUnlock": 1,
            "name": "图书任务再测试",
            "completeNum": 0,
            "totalNum": 1,
            "hasCorrect": 0,
            "isRead": 0,
            "taskItem": {
                "taskId": 126,
                "title": "图书图书图书图书图书图书图书图书图书图书图书图书图书图书图书",
                "source": 3,
                "taskStatus": 0,
                "taskType": 2
            }
        }]
    }, {
        "classStage": "课后学习",
        "classStageIcon": "https://tiku.huatu.com/cdn/images/ic/20200903/c7e387ebbb9343428b015a9e4b100675.png",
        "stageTasks": [{
            "id": 324,
            "type": "课后作业",
            "source": 1,
            "unlockTime": "09月16日 星期三 14:11",
            "isUnlock": 1,
            "name": "单任务 未上线",
            "completeNum": 0,
            "totalNum": 1,
            "hasCorrect": 0,
            "isRead": 0,
            "taskItem": {
                "taskId": 324,
                "taskItemId": 751,
                "materialId": 207,
                "mediaType": 3,
                "source": 1,
                "taskStatus": 0,
                "taskType": 2,
                "isOffline": 1
            }
        }, {
            "id": 325,
            "type": "课后作业",
            "source": 1,
            "unlockTime": "09月16日 星期三 14:12",
            "isUnlock": 1,
            "name": "多任务下线",
            "completeNum": 0,
            "totalNum": 2,
            "hasCorrect": 0,
            "isRead": 0
        }]
    }],
    "code": 1000000
}

compare_json_data(A, B)
