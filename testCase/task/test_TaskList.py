# coding=utf-8
import unittest
import random
import re
from base import Log
from base import common
from base import configHttp
from base import configDB
from jsonpath import jsonpath
from parameterized import parameterized

data_list = common.get_xls("testCase.xlsx", "task")
configHttp = configHttp.ConfigHttp()
log = Log.MyLog.get_log()
logger = log.get_logger()


class TaskList(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        """
        start
        :return:
        """
        logger.info(f'功能：{cls}\n')
        print("\n*******************测试开始*******************")
        cls.variable = {'token': common.get_token()}
        cls.db = configDB.OperateMysql()

    @parameterized.expand(data_list)
    def testTask(self, case_name, path, method, header, data, code, msg, sql, sql_assert, extract, extract_index):
        """
        test body
        :return:
        """
        # 变量替换
        if self.variable:
            path, method, header, data, sql = common.params_substitution([path, method, header, data, sql],
                                                                         self.variable)

        log.build_start_line(f"用例：{case_name}")
        print(f"\n1、用例：{case_name}")

        status_code, res = configHttp.http_method(method, path, header, data)
        log.res_info(status_code, res)
        # 提取变量
        print(f"6、提取变量：")
        if extract:
            v_name = extract.split('=')[0].strip()
            v_path = extract.split('=')[1].strip()
            try:
                extract_index = int(extract_index)
                extract_data = jsonpath(res, v_path)[extract_index]
                self.variable[v_name] = extract_data
                print("索引提取")
            except:
                extract_data = random.choice(jsonpath(res, v_path))
                self.variable[v_name] = extract_data
                print("随机提取")
            print(f"{v_name}： {extract_data}")
        # 断言结果
        try:
            print("7、开始断言：")
            self.assertEqual(status_code, 200, msg='请求失败')
            self.assertEqual(res['code'], int(code), msg='code断言失败')
            if res['code'] != 1000000:
                self.assertEqual(res['message'], msg, msg='msg断言失败')
            if sql:
                one_data = self.db.select_one_data(sql)
                self.assertEqual(one_data, sql_assert, msg='sql断言失败')
            print('断言succeed')
        except AssertionError:
            raise
        finally:
            log.build_end_line(f"用例：{case_name}")

    @classmethod
    def tearDownClass(cls):
        """
        end
        :return:
        """
        cls.db.conn_close()
        print("*******************测试结束*******************")


if __name__ == '__main__':
    unittest.main(verbosity=2)
